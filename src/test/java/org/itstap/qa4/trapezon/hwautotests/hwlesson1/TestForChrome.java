package org.itstap.qa4.trapezon.hwautotests.hwlesson1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestForChrome {
    public static void main(String[] args){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromewebdriver.exe");
        WebDriver driver= new ChromeDriver();
        driver.get("https://www.wikipedia.org/");
        driver.manage().window().maximize();
    }
}
