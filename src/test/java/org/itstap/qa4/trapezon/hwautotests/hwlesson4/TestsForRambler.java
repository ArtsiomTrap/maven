package org.itstap.qa4.trapezon.hwautotests.hwlesson4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestsForRambler {
        private WebDriver driver;

    @BeforeClass
    public void createWebDriver(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openStartPage(){
        driver.get("https://id.rambler.ru/login-20/mail-registration?back=https%3A%2F%2Fwww.rambler.ru&rname=head&type=self");
    }

    @Test(dataProviderClass = DataProviders.class,dataProvider ="dataProviderCorrectPasswords")
    public void correctPassword(String password){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("login")).sendKeys("asdfasfasdfgsdgsd");
        driver.findElement(By.id("newPassword")).sendKeys(password);
        driver.findElement(By.id("confirmPassword")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertNotEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div"))
                .getText(),"Пароль должен содержать от 8 до 32 символов, включать хотя бы одну заглавную латинскую букву, одну строчную и одну цифру");

    }

    @Test(dataProviderClass = DataProviders.class,dataProvider ="dataProviderIncorrectPasswords")
    public void incorrectPassword(String incorrectPassword){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("login")).sendKeys("asdzcxdfgfgh");
        driver.findElement(By.id("newPassword")).sendKeys(incorrectPassword);
        driver.findElement(By.id("confirmPassword")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement element= driver
                .findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div"));
        Assert.assertTrue(element.isDisplayed());
        Assert.assertNotEquals(element.getText(),"Пароль средней сложности");
        Assert.assertNotEquals(element.getText(),"Сложный пароль");
    }

    @Test
    public void passwordConfirm(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("login")).sendKeys("asdzcxdfgfgh");
        driver.findElement(By.id("newPassword")).sendKeys("A1qweasd");
        driver.findElement(By.id("confirmPassword")).sendKeys("A1qweasd");
        driver.findElement(By.id("answer")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.cssSelector("#root > div > div > div > article > form > div:nth-child(4) > section:nth-child(2) > div > div > div.rui-Input-root.rui-Input-awesome.rui-Input-withOutline.rui-Input-withStatusLine.rui-Input-isEnabled.rui-Input-medium.rui-Input-withEye"))
                .isDisplayed();
    }

    @Test(dataProviderClass = DataProviders.class,dataProvider ="dataProvideCorrectEmailName")
    public void correctEmailName(String emailName){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("login")).sendKeys(emailName);
        driver.findElement(By.id("newPassword")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.cssSelector("#root > div > div > div > article > form > div:nth-child(3) > section > div > div > div > div.c0219.rui-Input-root.rui-Input-awesome.rui-Input-withOutline.rui-Input-withStatusLine.rui-Input-isEnabled.rui-Input-medium.rui-Input-startPosition.rui-Input-inGroup"))
                .isDisplayed();

    }

    @Test(dataProviderClass = DataProviders.class,dataProvider ="dataProviderIncorrectEmailName")
    public void incorrectEmailName(String emailName){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("login")).sendKeys(emailName);
        driver.findElement(By.id("newPassword")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.cssSelector("#root > div > div > div > article > form > div:nth-child(3) > section > div > div > div.c0218.rui-FieldGroup-root.rui-FieldGroup-awesome.rui-FieldGroup-regular.rui-FieldGroup-showDivider > div.c0219.rui-Input-root.rui-Input-awesome.rui-Input-withOutline.rui-Input-withStatusLine.rui-Input-error.rui-Input-isEnabled.rui-Input-medium.rui-Input-startPosition.rui-Input-inGroup"))
                .isDisplayed();

    }


    @AfterClass
    public void closeBrowser(){
        driver.manage().deleteAllCookies();
        driver.close();
    }
}
