package org.itstap.qa4.trapezon.hwautotests.hwlesson4;

import org.testng.annotations.DataProvider;

public class DataProviders {
    @DataProvider(name = "dataProviderCorrectPasswords")
    public Object[][] dataProviderCorrectPasswords() {
        return new Object[][]{
                {"asdqweA1"},
                {"A1qqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"},
                {"!@$%^&*()_-+aA1"},
        };
    }

    @DataProvider(name = "dataProviderIncorrectPasswords")
    public Object[][] dataProviderIncorrectPasswords() {
        return new Object[][]{
                {"assdddd"},
                {"asaaaaaaaaasaaaaaaaaasaaaaaaaaxxs"},
                {"aaaaaasaaaaaaaaxxs"},
                {"aasaaaaaaaaxxsA"},
                {""},
                {"   "},
                {" afdsa dfaA2 "},
                {"#/;:\"'\\`"},
                {"123423421364234"},
                {"asa123123123"},
                {"AS3248672534867213"},
                {"ыфваыфвавафываФ2"},
                {"JHKHJKHJK"},

        };
    }

    @DataProvider(name = "dataProviderIncorrectEmailName")
    public Object[][] dataProviderIncorrectEmailName() {
        return new Object[][]{
                {"as"},
                {"asaaaaaaaaasaaaaaaaaasaaaaaaaaxxs"},
                {""},
                {"   "},
                {"!@$%^&*()+#/;:\"'\\`"},
                {"ыфваыфвавафываФ2"},
                {"asa__dfgdf"},
                {"_asadfgdf-"},
        };
    }

    @DataProvider(name = "dataProvideCorrectEmailName")
    public Object[][] dataProviderCorrectEmailName() {
        return new Object[][]{
                {"qqy"},
                {"qweqweqweqqweqweqweqqweqweqweqqq"},
                {" GFD-JHG_KJHG.JHG123asd "},

        };
    }



}
