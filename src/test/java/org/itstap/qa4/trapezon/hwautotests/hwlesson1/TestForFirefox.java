package org.itstap.qa4.trapezon.hwautotests.hwlesson1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestForFirefox {
    public static void main(String[] args){
        System.setProperty("webdriver.gecko.driver", "src/main/resources/firefoxwebdriver.exe");
        WebDriver driver= new FirefoxDriver();
        driver.get("https://www.wikipedia.org/");
        driver.manage().window().maximize();
    }

}
