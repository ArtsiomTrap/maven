package org.itstap.qa4.trapezon.hwautotests.hwlesson2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestsForOnlinersRegistration {

    @Test
    public void incorrectEmail() {
        String controlValueOne="Регистрация";
        String controlValueTwo="Некорректный e-mail";
        String incorrectEmail="asdas@";
        String password="qwertyui";

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://profile.onliner.by/registration");
        WebElement element=driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[1]"));
        Assert.assertEquals(element.getText(), controlValueOne);
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div/div/input"))
                .sendKeys(incorrectEmail);
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div[1]/div/input"))
                .sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[1]/div/input"))
                .sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[9]/button"))
                .click();
        element=driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText(), controlValueTwo);
    }

    @Test
    public void mismatchedPasswords() {
        String controlValueOne="Регистрация";
        String controlValueTwo="Пароли не совпадают";
        String email="asdas@gmail.com";
        String passwordFirst="qwertyui";
        String passwordSecond="123124";

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://profile.onliner.by/registration");
        WebElement element=driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[1]"));
        Assert.assertEquals(element.getText(), controlValueOne);
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div/div/input"))
                .sendKeys(email);
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div[1]/div/input"))
                .sendKeys(passwordFirst);
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[1]/div/input"))
                .sendKeys(passwordSecond);
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[9]/button"))
                .click();
        element=driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText(), controlValueTwo);
    }

    @Test
    public void emptyFields() {
        String controlValueOne="Укажите e-mail";
        String controlValueTwo="Укажите пароль";



        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://profile.onliner.by/registration");
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[9]/button"))
                .click();
        WebElement element=driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText(), controlValueOne);
        element=driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText(), controlValueTwo);
        element=driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div"));
        Assert.assertEquals(element.getText(), controlValueTwo);
    }
}
