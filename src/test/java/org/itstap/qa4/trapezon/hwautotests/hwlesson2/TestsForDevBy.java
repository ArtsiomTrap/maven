package org.itstap.qa4.trapezon.hwautotests.hwlesson2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestsForDevBy {
    @Test
    public void checkAllElements(){
        String controlValueOne="Вход";
        String controlValueTwo="Регистрация";
        String controlValueThree="Зарегистрироваться";
        String controlValueFour="Войти с помощью Google";
        String controlValueFive="Электронная почта";
        String controlValueSix="Пароль";
        String controlValueSeven="Юзернейм";
        String controlValueEight="Номер телефона";

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get("https://id.dev.by/@/welcome");
        WebElement element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/header/div/div[1]/a"));//на главную
        Assert.assertTrue(element.isDisplayed());
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[1]")); //Вход
        Assert.assertTrue(element.isDisplayed());
        Assert.assertTrue(element.isEnabled());
        Assert.assertEquals(element.getText(), controlValueOne);
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]")); //Регистрация
        Assert.assertTrue(element.isDisplayed());
        Assert.assertTrue(element.isEnabled());
        Assert.assertEquals(element.getText(), controlValueTwo);
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span/span")); //Кнопка зарегистрироваться
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getText(), controlValueThree);
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[4]/button/span/span")); //Войти с помошью гугл
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getText(), controlValueFour);
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input")); //поле почты
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getAttribute("placeholder"), controlValueFive);
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input")); //поле пароль
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getAttribute("placeholder"), controlValueSix);
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input")); //поле юзернейм
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getAttribute("placeholder"), controlValueSeven);
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input")); //поле телефон
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getAttribute("placeholder"), controlValueEight);
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[1]/label[2]")); // 1й чекбокс
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getText(), "Собираюсь размещать вакансии");
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[2]/label[2]")); // 2й чекбокс
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getText(), "Получать раз в неделю главные\n" +
                "ИТ-новости и анонсы мероприятий");
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[3]/label[2]")); // 3й чекбокс
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getText(), "Принимаю условия пользовательского соглашения");
        Assert.assertTrue(element.isEnabled());


        }

    @Test
    public void incorrectEmail(){
        String controlValue="Введите корректный адрес электронной почты.";
        String incorrectEmail="asdas@";
        String password="qwertyui";
        String userName="asdasda";
        String phoneNumber="2345345624356";

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://id.dev.by/@/welcome");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"))
                .sendKeys(incorrectEmail);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"))
                .sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"))
                .sendKeys(userName);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"))
                .sendKeys(phoneNumber);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span"))
                .click();
        WebElement element=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span"));
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getText(),controlValue);

    }

    @Test
    public void emptyPassword(){
        String controlValue="Введите пароль.";
        String eMail="asdas@gmail.com";
        String userName="asdasda";
        String phoneNumber="2345345624356";

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://id.dev.by/@/welcome");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"))
                .sendKeys(eMail);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"))
                .sendKeys(userName);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"))
                .sendKeys(phoneNumber);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span"))
                .click();
        WebElement element=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span"));
        Assert.assertTrue(element.isDisplayed());
        Assert.assertEquals(element.getText(),controlValue);

    }
}
