package org.itstap.qa4.trapezon.hwautotests.hwlesson3;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class TestsForMoscou {
    private WebDriver driver;

    @BeforeClass
    public void createWebDriver(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openStartPage(){
        driver.get("http://hflabs.github.io/suggestions-demo/");
    }

    @Test
    public void checkAllImportantElements(){
        WebElement element= driver.findElement(By.id("fullname"));
        Assert.assertTrue(element.isDisplayed());
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.id("email"));
        Assert.assertTrue(element.isDisplayed());
        Assert.assertTrue(element.isEnabled());
        element= driver.findElement(By.id("message"));
        Assert.assertTrue(element.isDisplayed());
        Assert.assertTrue(element.isEnabled());
        Assert.assertTrue(driver.findElement(By
                .xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/label")).isDisplayed());
        Assert.assertTrue(driver.findElement(By
                .xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[4]/label")).isDisplayed());
        Assert.assertTrue(driver.findElement(By
                .xpath("//*[@id=\"feedback-form\"]/div[2]/label")).isDisplayed());
    }

    @Test
    public void sendMessage(){
        driver.findElement(By.id("fullname")).sendKeys("Иванов Иван Петрович");
        driver.findElement(By.id("email")).sendKeys("sd@ddf.rr");
        driver.findElement(By.id("message")).sendKeys("asdasdasd");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertFalse(driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed());
    }


        @DataProvider(name = "dataProviderValidationEmail")
        public Object[][] dataProviderValidationEmail() {
            return new Object[][]{
                    {""},
                    {"     "},
                    {"asd"},
                    {"@asdf"},
                    {"asd@asd"},
                    {".asd@asd."},
                    {"-asd@asd-"},
                    {"ыва@ывфю.цу"},
                    {"asd asd@asd asd"},
                    {"wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww@fg.ru"},
                    {"asd@"}
            };
        }

    @Test(dataProvider = "dataProviderValidationEmail")
    public void validationEmail(String incorrectEmail){
        driver.findElement(By.id("fullname")).sendKeys("Иванов Иван Петрович");
        driver.findElement(By.id("message")).sendKeys("asdasdasd");
        driver.findElement(By.id("email"))
                .sendKeys(incorrectEmail);
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertFalse(driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed());
    }

    @Test
    public void checkAutoCompleteAddress(){
        WebElement element = driver.findElement(By.id("address"));
        element.sendKeys("г Москва, пр-кт Вернадского, д 4 стр 1, кв 67");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.sendKeys(Keys.ENTER);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(driver.findElement(By.id("address-postal_code"))
                .getAttribute("value"),"119334");
        Assert.assertEquals(driver.findElement(By.id("address-region"))
                .getAttribute("value"),"г Москва");
        Assert.assertEquals(driver.findElement(By.id("address-city"))
                .getAttribute("value"),"г Москва");
        Assert.assertEquals(driver.findElement(By.id("address-street"))
                .getAttribute("value"),"пр-кт Вернадского");
        Assert.assertEquals(driver.findElement(By.id("address-house"))
                .getAttribute("value"),"д 4, стр 1");
        Assert.assertEquals(driver.findElement(By.id("address-flat"))
                .getAttribute("value"),"кв 67");
    }


    @AfterClass
    public void closeBrowser(){
        driver.manage().deleteAllCookies();
        driver.close();
    }
}
